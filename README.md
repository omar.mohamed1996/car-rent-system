# Renting Cars Service
![](readme-images/pages/home.png)

## Table of contents
- [Overview](#Overview)
- [Scope](#Scope)
- [ERD Diagram](#ERD-Diagram)
- [Architecture](#Architecture)
- [Models](#Models)
- [Methods](#Methods)
- [Installation/Run](#Installation/Run) 
- [Screenshots](#Screenshots)

## Overview
Renting Cars Service allows you to fully control the entire process, monitor the loading of each car and provide the best service for your clients. With Renting Cars Service, you can manage reservations for any vehicle directly from our website.

## Scope

```plantuml
'left to right direction

skinparam usecase {
	BackgroundColor LightGrey
	BorderColor grey
	ArrowColor Grey

}
skinparam rectangle {
	BackgroundColor white
	BorderColor grey
	ArrowColor Grey

}
skinparam component {
	BackgroundColor LightGrey
	BorderColor grey
	ArrowColor Grey

}
skinparam shadowing false
skinparam linetype ortho

skinparam actor{
    	BackgroundColor LightGrey
    		BorderColor grey


}

Actor Customer
Actor Admin
Actor Driver

rectangle Web{
rectangle CustomerPortal as "Customer Portal"{
[Create Booking]
[Update Booking]
[Delete Booking]
[Payment Methods]
[View Invoices]
[Account Management]
[Feadback]
}


rectangle AdminPortal as "Admin Portal"{
[Admin Management]
[Dashboard]
[Customer Management]
[Car Management]
[Driver Management]
[Invoices Management]
[Car/Driver Booking]
[Feadback Portal]
}

rectangle DriverPortal as "Driver Portal"{
[Date/Time Management]
[View Travel Location]
[Customer Feadback]
[Edit Availability]
}

}

Admin --> AdminPortal
Customer --> CustomerPortal
Driver --> DriverPortal

```
## ERD-Diagram
![](readme-images/ERD_Diagram.png)

## Architecture

```plantuml
'up to down direction


skinparam rectangle {
	BackgroundColor white
	BorderColor grey
	ArrowColor Grey

}
skinparam component {
	BackgroundColor LightGrey
	BorderColor grey
	ArrowColor Grey

}
skinparam shadowing false
skinparam linetype ortho

skinparam actor{
    	BackgroundColor LightGrey
    	BorderColor grey

}
rectangle "Azure/Google/AWS/eFinance/On-Premise-Servers" as platform{
    rectangle "Docker-Compose"{
        
        rectangle "<&globe>FrontEnd" as FrontEnd{
                rectangle "<&globe>HTML" as HTML{
                }
                rectangle "<&globe>CSS" as CSS{
                }
                rectangle "<&globe>JavaScript" as JavaScript{
                }
        }
        
        rectangle BackEnd{
            rectangle Python{
                [<&code>Flask] 
                [<&code>Gunicorn] as Gunicorn
            }
        }
        
        rectangle "<&hard-drive>MySQL" as MySQL
        rectangle "<&globe>NGinx" as NGinx{
        }
    }
}
NGinx -> FrontEnd
NGinx -> BackEnd
BackEnd <--> MySQL

```
## Models
(some examples)
```sql
CREATE TABLE `Admin_User` (
  `userId` varchar(100) NOT NULL,
  `fName` varchar(100) DEFAULT NULL,
  `lName` varchar(100) DEFAULT NULL,
  `emailId` varchar(100) DEFAULT NULL,
  `phone` varchar(100) DEFAULT NULL,
  `registration_Date` varchar(100) DEFAULT NULL,
  `password` varchar(1000) DEFAULT NULL,
  `reset_Question` varchar(100) DEFAULT NULL,
  `reset_Ans_Type` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1
```
```sql
CREATE TABLE `Booking` (
  `bookingId` int(50) NOT NULL AUTO_INCREMENT,
  `userId` varchar(100) DEFAULT NULL,
  `Cab` varchar(100) DEFAULT NULL,
  `startDate` varchar(100) DEFAULT NULL,
  `endDate` varchar(100) DEFAULT NULL,
  `Pickup_time` varchar(100) DEFAULT NULL,
  `Pickup_location` varchar(100) DEFAULT NULL,
  `Drop_off_location` varchar(100) DEFAULT NULL,
  `driverId` int(50) DEFAULT NULL,
  `carid` varchar(100) DEFAULT NULL,
  `cab_route` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`bookingId`),
  KEY `userId` (`userId`),
  KEY `driverId` (`driverId`),
  CONSTRAINT `Booking_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `Cust_User` (`userId`),
  CONSTRAINT `Booking_ibfk_2` FOREIGN KEY (`driverId`) REFERENCES `Driver` (`driverId`)
) ENGINE=InnoDB AUTO_INCREMENT=60 DEFAULT CHARSET=latin1;
```
```sql
CREATE TABLE `Car` (
  `Car_id` varchar(100) NOT NULL,
  `model_name` varchar(100) DEFAULT NULL,
  `registeration_no` varchar(100) DEFAULT NULL,
  `seating_capacity` varchar(100) DEFAULT NULL,
  `Car_type` varchar(100) DEFAULT NULL,
  `price_per_km` varchar(100) DEFAULT NULL,
  `status` varchar(100) DEFAULT 'Available',
  PRIMARY KEY (`Car_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
```

## Methods
(some examples)
```python
@app.route('/bookingNow/',methods = ['GET','POST'])
def booking():
	print("entered")
	cabs_list = ["Hatchback","Sedan","SUV"]
	buflag = False
	try:
		cab_route = ['Cairo-Alex','Cairo-Giza','Cairo-ghardaqa','Cairo-AinSokhna','Cairo-Ismailia']
		car_empty = False
		driver_empty = False
		userId=request.form["userId"]
		cursor.execute("""SELECT userId FROM Cust_User""")
		busernames = cursor.fetchall()
		busernames_list = list(busernames)
		busernames_len = len(busernames_list)
		for a in range(0,busernames_len):
			if busernames_list[a][0] == userId:
				buflag = True
		if buflag == False:
			flash("Entered Username does not Exist !!!")
			return render_template('booking.html')
		
		cursor.execute("""SELECT fName,lName,emailId,phone FROM Cust_User WHERE userId = %s""",[userId])
		custinfo = cursor.fetchall()
		custinfo_list = list(custinfo)
		print(custinfo_list)
		fname = custinfo_list[0][0]
		lname = custinfo_list[0][1]
		email = custinfo_list[0][2]
		phone = custinfo_list[0][3]
		print(fname,lname,email,phone)
		'''fname = request.form["fName"]
		print(fname)
		lname = request.form["lName"]
		phone = request.form["PhoneNumber"]
		email = request.form["email"]	'''
		cab1 = request.form["cab"]
		cab = int(cab1)
		print(userId,fname,lname,phone,email,cab)
		cab_name = ""
		route = ""
		cab_name = cabs_list[cab]
		startDate = request.form["startDate"]
		endDate = request.form["endDate"]
		time = request.form["time"]
		carroute = int(request.form["route"])
		route = cab_route[carroute]
		pickupLocation = request.form["pickupLocation"]
		dropoffLocation = request.form["dropoffLocation"]
		pricePerKm = 10;
		car_name = cabs_list[cab]
	
		print(userId,fname,lname,phone,email,cab,startDate,endDate,time,pickupLocation,dropoffLocation,route)
		
		cursor.execute("""SELECT Car_id FROM Car WHERE status = 'Available' and Car_type = %s """,[car_name])
		car = cursor.fetchall()
		print(car)
		if car:
			print('Structure is not empty.')
			car_empty = False
		else:
			print('Structure is empty.')
			return redirect('/allbooked')
			car_empty =  True
		carid = car[0][0]
		
		cursor.execute("""SELECT driverId FROM Driver WHERE status = 'Available' """)
		driver = cursor.fetchall()
		if driver:
			print('Structure is not empty.')
			car_empty = False
		else:
			print('Structure is empty.')
			return redirect('/allbooked')
			car_empty =  True
		driverid = driver[0][0]
		print("NULL CARID" ,carid)
		print("NULL driverid",driverid)
		cursor.execute("""UPDATE Car SET status = "BOOKED" WHERE Car_id = %s""",[carid])
		global CARID
		CARID = carid[:]
		global driverID
		driverID = driverid
		
		cursor.execute("""UPDATE Driver SET status = "BOOKED" WHERE driverId = %s""",[driverid])
		print("DRIVER ID : ",driverid)
		cursor.execute("""INSERT INTO Booking(userId,Cab,startDate,endDate,Pickup_time,Pickup_location,Drop_off_location,driverId,carid,cab_route) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)""",(userId,cab_name,startDate,endDate,time,pickupLocation,dropoffLocation,driverid,carid,route)) 
		
		conn.commit()
		global b_actual_id
		cursor.execute("""select bookingId from Booking where Pickup_time=%s and startDate=%s """,[time,startDate])
		b_id=cursor.fetchall()
		b_actual_id=b_id[0][0]
		print(b_actual_id)  
		print ("Booked")		
		return redirect("/payment")
		
	except Exception as e:
		return(str(e))
```
## Installation/Run
### 1. Manual Installation

#### 1.1 MySQL Ubuntu Installation/Configurations

##### 1. Installation
```bash
sudo apt install mysql-server
sudo systemctl start mysql.service
sudo mysql
```
##### 2. Configurations

```sql
create USER 'root'@'localhost' IDENTIFIED WITH authentication_plugin BY 'password';
create database car_rent_system;
```
```bash
mysql -u root -p car_rent_system < ./car_rental_db.sql
```
###### 1.2 Clone the Repo
```bash
git clone https://gitlab.com/omar.mohamed1996/car-rent-system.git
```
###### 1.3. Install Python (Python 3) dependencies
###### 1.3.1. Python virtualenv
If you don't want to install dependencies globally, then it is recommended to use a Python virtual environment

Virtualenv is a tool used to create an isolated Python environment. This environment has its own installation directories that doesn't share libraries with other virtualenv environments (and 
optionally doesn't access the globally installed libraries either).

Virtualenv is the easiest and recommended way to configure a custom Python environment. Otherwhise, jump to step 3.2

###### 1 Install Virtualenv:
``` bash
pip install virtualenv
```

###### 2. Create virtual environment:
``` bash
virtualenv ENV
```
Where ENV is a directory in which to place the new virtual environment (example: C:\documents\my_env_folder).

###### 3. Activate virtual environment:
On macOS and Linux:
```bash
source env/bin/activate
```
On windows systems, the activation script is found on ENV\Scripts\activate.bat so just run
```bash
C:\path\to\ENV\Scripts\activate.bat
```
or
```bash
.\env\Scripts\activate
```

###### 4. Deactivate virtual environment:
(whenever you want to stop using the virtualenv)
```bash
deactivate
```

###### 1.3.2 Installing Python modules
To install all required modules, just run:
```bash
pip install -r requirements.txt
```
###### 1.4. Run Application
```bash
python main.py
```

### 2. Docker-Compose Installation
#### 2.1 MySQL Ubuntu Installation/Configurations

##### 1. Installation
```bash
sudo apt install mysql-server
sudo systemctl start mysql.service
sudo mysql
```
##### 2. Configurations

```sql
create database car_rent_system;
```
```bash
mysql --host=127.0.0.1 --port=32000 -u root -p car_rent_system< ./car_rental_db.sql
```

#### 2.2 Run Application
```bash
sudo docker-compose up
```
you can install docker, docker-compose through this link https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-compose-on-ubuntu-20-04

## Screenshots
![Home Page](readme-images/pages/home.png)
![AboutUS Page](readme-images/pages/about.png)
![FeedBack Page](readme-images/pages/feedback.png)
![Customer Register Page](readme-images/pages/customer_register.png)
![Login Page](readme-images/pages/login.png)
![Booking Page](readme-images/pages/booking.png)
![Payment Page](readme-images/pages/payment_methods.png)
![Invoice Page](readme-images/pages/invoice_details.png)admin_dashboard
![Admin Register Page](readme-images/pages/admin_register.png)
![Admin Portal](readme-images/pages/admin_d![FeedBack Page](readme-images/pages/feedback.png)
![Customer Register Page](readme-images/pages/customer_register.png)
![Login Page](readme-images/pages/login.png)
![ForgetPassword Page](readme-images/pages/forget-password.png)

![Booking Page](readme-images/pages/booking.png)
![Payment Page](readme-images/pages/payment_methods.png)
![Invoice Page](readme-images/pages/invoice_details.png)admin_dashboard
![Admin Register Page](readme-images/pages/admin_register.png)
![Admin Portal](readme-images/pages/admin_portal.png)
![Admin Dashboard](readme-images/pages/admin_dashboard.png)
![Admin Management](readme-images/pages/display_all_admins.png)
![Remove Admin](readme-images/pages/remove_admin.png)
![Customer Management](readme-images/pages/customer_details.png)
![Remove Customer](readme-images/pages/remove_customer.png)
![Driver Management](readme-images/pages/divers_details.png)
![Add Driver](readme-images/pages/add_new_driver.png)
![Remove Driver](readme-images/pages/remove_driver.png)
![Edit Driver Availability](readme-images/pages/drivers_availability.png)
![Car Management](readme-images/pages/car_availability.png)
![Add Car](readme-images/pages/add_new_car.png)
![Edit Car Availability](readme-images/pages/edit_car_availability.png)
![Booking Management](readme-images/pages/all_booking_details.png)

![FeedBacks](readme-images/pages/feadbacks_portal.png)

![Login History](readme-images/pages/login_history.png)


